package se.nackademin.mygame;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;

public class Bullets extends Objects{

	//lifetime is to find how long the bullet is on screen
	private float bulletTime;
	private float bulletTimer;

	//remove when the bullet time is ended
	private boolean remove;

	public Bullets(float x, float y, float radians){

		this.x = x;
		this.y = y;
		this.radians = radians;

		float speed = 350;
		vdirx = MathUtils.cos(radians) * speed;
		vdiry = MathUtils.sin(radians) * speed;

		width = height = 4;

		bulletTimer = 0;
		bulletTime = 1.25f; //should only stay on screen for  1,25 sec
	}

	public boolean removeBullet(){
		return remove;
	}

	public void update(float dt){

		x += vdirx * dt;
		y += vdiry * dt;

		wrap();

		bulletTimer += dt;
		if(bulletTimer > bulletTime){
			remove = true;
		}
	}

	public void draw(ShapeRenderer sr){
		sr.setColor(1,1,1,1);
		sr.begin(ShapeType.Filled);
		sr.circle(x- width / 2, y - height /2, width / 2);
		sr.end();

	}
}
