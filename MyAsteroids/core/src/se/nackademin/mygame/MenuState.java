package se.nackademin.mygame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class MenuState extends GameState{
	
	private final String title = "ASTEROIDS";
	
	private BitmapFont fontTitle;
	private BitmapFont fontMenu;
	
	private SpriteBatch sprite;
	
	private int currentMenuSelect;
	private String[] menuSelect;
	
	public MenuState(GameStateHandling gState){
		super(gState);
	}

	public void init() {
		sprite = new SpriteBatch();
		
		FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/slkscr.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 94;
		fontTitle = gen.generateFont(parameter);
		
		FreeTypeFontParameter parameter2 = new FreeTypeFontParameter();
		parameter2.size = 40;
		fontMenu = gen.generateFont(parameter2);
		
		gen.dispose();
		
		menuSelect = new String[]{"PLAY", "QUIT"};
		
	}

	public void update(float dt) {
		
		handleInput();
	}

	public void draw() {
		
		sprite.setProjectionMatrix(AsteroidsGame.cam.combined);
		sprite.setColor(1, 1, 1, 1);
		sprite.begin();
		
		//Draw title
		float width = fontTitle.getBounds(title).width;
		fontTitle.draw(sprite, title, (AsteroidsGame.WIDTH - width) / 2, 500);
		
		//draw menu
		for(int i = 0; i < menuSelect.length; i++){
			width = fontMenu.getBounds(menuSelect[i]).width;
			if(currentMenuSelect == i) fontMenu.setColor(0, 1, 0, 1);
			else fontMenu.setColor(1, 1, 1, 1);
			fontMenu.draw(sprite, menuSelect[i], (AsteroidsGame.WIDTH - width) / 2, 400 - 60 * i); //-35 * i, moves the menu items down per i
		}
		
		sprite.end();
	}

	public void handleInput() {
		//control the menu
		if(GameKeys.isPressed(GameKeys.UP)){
			if(currentMenuSelect > 0){
				currentMenuSelect--;
			}
		}
		if(GameKeys.isPressed(GameKeys.DOWN)){
			if(currentMenuSelect < menuSelect.length - 1){
				currentMenuSelect++;
			}
		}
		if(GameKeys.isPressed(GameKeys.ENTER)){
			select();
		}
		
	}
	private void select(){
		if(currentMenuSelect == 0){
			gState.setState(GameStateHandling.PLAY);
		}
		else if(currentMenuSelect == 1){
			Gdx.app.exit();
		}
		
	}

	public void dispose() {
		sprite.dispose();
		fontMenu.dispose();
		fontTitle.dispose();
	}

}
