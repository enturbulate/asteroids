package se.nackademin.mygame;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;

public class Ship extends Objects{

	private float[] thrustx;
	private float[] thrusty;

	//movement
	private boolean up; //acceleration when up is pressed
	private boolean left;  //rotation when left key is pressed
	private boolean right;

	//maxspeed for speed limit
	private float maxSpeed;
	private float acceleration;
	private float deceleration;
	private float accelTimer;

	private final int MAX_BULLETS = 4;
	private ArrayList<Bullets> bullets;

	private boolean hit;
	private boolean dead;
	private float hitTimer;
	private float hitTime;
	private Line2D.Float[] deadLines;
	private Point2D.Float[] deadLinesVector;

	private int extraLives;
	private long extraLifeScore;
	private long score;

	public Ship(ArrayList<Bullets> bullets){

		this.bullets = bullets;

		x = AsteroidsGame.WIDTH / 2;
		y = AsteroidsGame.HEIGHT / 2;

		//value is pixels per seconds
		maxSpeed = 300;
		acceleration = 200;
		deceleration = 10;

		//ship shape, using 4 points on the ship,
		pshapex = new float[4];
		pshapey = new float [4];
		//flame shape
		thrustx = new float[3];
		thrusty = new float[3];

		//player ship facing upwards, radians is the direction
		radians = (float) (Math.PI / 2);
		rotSpeed = 3;

		hit = false;
		hitTimer = 0;
		hitTime = 2; //2 seconds

		score = 0;
		extraLives = 3;
		extraLifeScore = 5000;

	}

	private void setShape(){
		//the top radians is 12 pixels away from the middle
		pshapex[0] = x + MathUtils.cos(radians) * 10;
		pshapey[0] = y + MathUtils.sin(radians) * 10;
		//lower left point
		pshapex[1] = x + MathUtils.cos((float) (radians - 4 * Math.PI / 5)) * 12;
		pshapey[1] = y + MathUtils.sin((float) (radians - 4 * Math.PI / 5)) * 12;
		//lower mid point
		//is opposite the top 5 pixels away from the mid
		pshapex[2] = x + MathUtils.cos((float) (radians + Math.PI)) * 7;
		pshapey[2] = y + MathUtils.sin((float) (radians + Math.PI)) * 7;
		//lower right point
		pshapex[3] = x + MathUtils.cos((float) (radians + 4 * Math.PI / 5)) * 12;
		pshapey[3] = y + MathUtils.sin((float) (radians + 4 * Math.PI / 5)) * 12;
	}

	private void setThrust(){
		thrustx[0] = x + MathUtils.cos((float) (radians - 5 * Math.PI / 6)) * 12;
		thrusty[0] = y + MathUtils.sin((float) (radians - 5 * Math.PI / 6)) * 12;

		thrustx[1] = x + MathUtils.cos((float) (radians - Math.PI)) * (8 + accelTimer * 90);
		thrusty[1] = y + MathUtils.sin((float) (radians - Math.PI)) * (8 + accelTimer * 90);

		thrustx[2] = x + MathUtils.cos((float) (radians + 5 * Math.PI / 6)) * 12;
		thrusty[2] = y + MathUtils.sin((float) (radians + 5 * Math.PI / 6)) * 12;
	}

	public void setLeft(boolean b){left = b;}
	public void setRight(boolean b){right = b;}
	public void setUp(boolean b){up = b;}

	public void setPosition(float x, float y){
		super.setPosition(x, y);
		setShape();
	}

	public boolean isHit(){
		return hit;
	}
	public boolean isDead(){
		return dead;
	}
	public void reset(){
		//reset the player pos to the center and get rid of the hit and dead flags
		x = AsteroidsGame.WIDTH / 2;
		y = AsteroidsGame.HEIGHT / 2;
		setShape();
		hit = dead = false;
	}

	public long getScore(){
		return score;
	}
	public int getLives(){
		return extraLives;
	}

	public void loseLife(){
		extraLives --;
	}
	//get score when we shoot an asteroid
	public void updateScore(long l){
		score += l;
	}


	public void shoot(){
		if(bullets.size() == MAX_BULLETS) return; 
		bullets.add(new Bullets(x, y, radians));
	}

	public void hit(){

		if(hit) return;

		hit = true;
		//stop player from moving, and let go of the keys
		vdirx = vdiry = 0;
		left = right = up = false;

		//deadLines is for explosion, 4 because of 4 edges on the player polygon
		deadLines = new Line2D.Float[4];
		for(int i = 0, j = deadLines.length - 1;
				i < deadLines.length;
				j = i++){
			deadLines[i] = new Line2D.Float(pshapex[i], pshapey[i], pshapex[j], pshapey[j]);
		}

		//deadLinesVector is for which direction the lines are going in explosion.
		deadLinesVector = new Point2D.Float[4];
		deadLinesVector[0] = new Point2D.Float(
				MathUtils.cos(radians + 1.5f), MathUtils.sin(radians + 1.5f));
		deadLinesVector[1] = new Point2D.Float(
				MathUtils.cos(radians - 1.5f), MathUtils.sin(radians - 1.5f));
		deadLinesVector[2] = new Point2D.Float(
				MathUtils.cos(radians - 2.8f), MathUtils.sin(radians - 2.8f));
		deadLinesVector[3] = new Point2D.Float(
				MathUtils.cos(radians + 2.8f), MathUtils.sin(radians + 2.8f));

	}

	public void update(float dt){

		//check if hit
		if(hit){
			hitTimer += dt;
			//if hitTimer > hitTime, we're dead, and we need to respawn
			if(hitTimer > hitTime){
				dead = true;
				hitTimer = 0;
			}
			for(int i = 0; i < deadLines.length; i++){
				deadLines[i].setLine(
						deadLines[i].x1 + deadLinesVector[i].x * 10 * dt,
						deadLines[i].y1 + deadLinesVector[i].y * 10 * dt,
						deadLines[i].x2 + deadLinesVector[i].x * 10 * dt,
						deadLines[i].y2 + deadLinesVector[i].y * 10 * dt);
			}
			return;
		}

		//check extra lives
		if(score >= extraLifeScore){
			extraLives++;
			extraLifeScore += 10000;
		}

		//turning
		if(left){
			radians += rotSpeed * dt;
		}
		else if(right) {
			radians -= rotSpeed * dt;
		}

		//accelerating
		if(up){
			vdirx += MathUtils.cos(radians) * acceleration * dt;
			vdiry += MathUtils.sin(radians) * acceleration * dt;

			//for the flashing animation of the thrust animation
			accelTimer += dt;
			if(accelTimer > 0.1f){
				accelTimer = 0;
			}
		}
		else{
			accelTimer = 0;
		}

		//deceleration
		float vec = (float) Math.sqrt(vdirx * vdirx + vdiry * vdiry);
		if(vec > 0) {
			vdirx -= (vdirx / vec) * deceleration * dt;
			vdiry -= (vdiry / vec) * deceleration * dt;
		}
		if(vec > maxSpeed){
			vdirx = (vdirx / vec) * maxSpeed;
			vdiry = (vdiry / vec) * maxSpeed;
		}

		//set position
		x += vdirx * dt;
		y += vdiry * dt;

		//set shape
		setShape();

		//set flame
		if(up){
			setThrust();
		}

		// screen wrap
		wrap();

	}

	public void draw(ShapeRenderer sr){

		//set color to white
		sr.setColor(1,1,1,1);

		//all drawing has to go between begin - end
		sr.begin(ShapeType.Line);

		//check if hit
		//if we're hit, we're just gonna draw the hitlines, not the ship or flame
		if(hit){
			for(int i = 0; i < deadLines.length; i++){
				sr.line(deadLines[i].x1, deadLines[i].y1, deadLines[i].x2, deadLines[i].y2);
			}
			sr.end();
			return;

		}

		//draw ship
		for(int i = 0, j = pshapex.length - 1;
				i < pshapex.length;
				j = i++){
			sr.line(pshapex[i], pshapey[i], pshapex[j], pshapey[j]);
		}

		//draw flames
		if(up){
			for(int i = 0, j = thrustx.length - 1;
					i < thrustx.length;
					j = i++){
				sr.line(thrustx[i], thrusty[i], thrustx[j], thrusty[j]);
			}
		}



		sr.end();

	}

}





