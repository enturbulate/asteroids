package se.nackademin.mygame;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;

public class Asteroids extends Objects {

	//asteroids size, large splits into medium etc
	public static final int SMALL = 1;
	public static final int MEDIUM = 2;
	public static final int LARGE = 3;  //these variables need to be set to different values, the value itself doesn't matter.
	
	//for generating random asteroids (forms)
	private int numberPoints;
	private float[] centerDistance;
	
	private int type;
	private int score;
	private boolean remove;

	public Asteroids(float x, float y, int type){

		this.x = x;
		this.y = y;
		this.type = type;

		if(type == SMALL){
			numberPoints = 10; //how many points the vertex has
			width = height = 20;
			speed = MathUtils.random(90, 120); 
			score = 100;
		}
		else if (type == MEDIUM){
			numberPoints = 12;
			width = height = 40;
			speed = MathUtils.random(70, 80);
			score = 50;
		}
		else if (type == LARGE){
			numberPoints = 15;
			width = height = 80;
			speed = MathUtils.random(40, 50);
			score = 20;
		}

		rotSpeed = MathUtils.random(-1, 1);

		//random direction for asteroids
		radians = MathUtils.random((float) (2 * Math.PI));
		//set vector cos and sin
		vdirx = MathUtils.cos(radians) * speed;
		vdiry = MathUtils.sin(radians) * speed;

		pshapex = new float[numberPoints];
		pshapey = new float[numberPoints];
		centerDistance = new float[numberPoints];

		int radius = width / 2;
		//generating random distances from center
		for(int i = 0; i < numberPoints; i++){ 
			centerDistance[i] = MathUtils.random(radius / 2, radius);
		}

		setShape();

	}

	private void setShape(){
		float angle = 0;
		for(int i = 0; i < numberPoints; i++){
			pshapex[i] = x + MathUtils.cos(angle + radians) * centerDistance[i];
			pshapey[i] = y + MathUtils.sin(angle + radians) * centerDistance[i];
			angle += 2 * Math.PI / numberPoints;
		}
	}

	public int getType(){
		return type;
	}

	public boolean shouldRemove(){
		return remove;
	}
	
	public int getScore(){
		return score;
	}

	public void update(float dt){

		x += vdirx * dt;
		y += vdiry * dt;

		radians += rotSpeed * dt;
		setShape();

		wrap();

	}

	public void draw(ShapeRenderer sr){
		sr.setColor(1, 1, 1, 1);
		sr.begin(ShapeType.Line);
		for(int i = 0, j = pshapex.length - 1;
				i < pshapex.length;
				j = i++){
			sr.line(pshapex[i], pshapey[i], pshapex[j], pshapey[j]);
		}

		sr.end();

	}


}
