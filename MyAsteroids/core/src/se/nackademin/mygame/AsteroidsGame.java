package se.nackademin.mygame;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class AsteroidsGame extends ApplicationAdapter {

	public static OrthographicCamera cam;
	private GameStateHandling gState;
	
	public static int WIDTH;
	public static int HEIGHT;

	@Override
	public void create () {
		WIDTH = Gdx.graphics.getWidth();
		HEIGHT = Gdx.graphics.getHeight();
		cam = new OrthographicCamera(WIDTH, HEIGHT);
		cam.translate(WIDTH / 2, HEIGHT / 2);
		cam.update();

		Gdx.input.setInputProcessor(new KeyInputs());

		gState = new GameStateHandling();
	}

	@Override
	public void render () {

		//clears screen to black 
		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		//delta time shows how long its been since last time render was called.
		//used for seeing how much time we want to move the game forward
		gState.update(Gdx.graphics.getDeltaTime());
		gState.draw();

		GameKeys.update();
	}
	@Override
	public void pause(){ } //mostly for android, when phone call, pause
	@Override
	public void resume(){ } //same as above, but resume


	@Override
	public void dispose(){

	}

}
