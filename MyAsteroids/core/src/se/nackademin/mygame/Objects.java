package se.nackademin.mygame;


public class Objects {

	//position
	protected float x;
	protected float y;
	//vector direction stuff is traveling
	protected float vdirx;
	protected float vdiry;

	//size
	protected int width;
	protected int height;
	
	//direction the angle we're facing
	protected float radians;

	protected float speed;
	protected float rotSpeed;

	//shape polygon vertices
	protected float[] pshapex;
	protected float[] pshapey;

	//getters for x and y;
	public float getx(){
		return x;
	}
	public float gety(){
		return y;
	}

	//getters for pshapex and pshapey (polygon vertices)
	public float[] getpShapex(){
		return pshapex;
	}
	public float[] getpShapey(){
		return pshapey;
	}

	public void setPosition(float x, float y){
		this.x = x;
		this.y = y;
	}

	//x and y to check if it intersects with  pshapex and pshapey
	//even-odd rule (basically the if-statement)
	//algorithm used in vector based graphic software to 
	//determine the "insideness" of a point, in this game it's used for
	//collision detection.
	public boolean collisionDetect (float x, float y){
		boolean b = false;
		for(int i = 0, j = pshapex.length - 1;
				i < pshapex.length;
				j = i++){
			if((pshapey[i] > y) != (pshapey[j] > y) && 
					(x < (pshapex[j] - pshapex[i]) *
							(y - pshapey[i]) / (pshapey[j] - pshapey[i])
							+ pshapex[i])){
				//flip the boolean, if it's even it's outside the polygon, if it's odd it's inside
				b = !b;
			}
		}
		return b;
	}

	//checking to see if Objects polygons intersects (asteroids and ship)
	//using the collisionDetect method to see if the points in the entire polygon
	//intersects with the other polygon
	public boolean intersects(Objects other){
		float[] sx = other.getpShapex();
		float[] sy = other.getpShapey();
		for(int i = 0; i < sx.length; i++){
			if(collisionDetect(sx[i], sy[i])){
				return true;
			}
		}
		return false;
	}

	//screen wrapper, when an object exits the screen it appears on the other end
	protected void wrap(){
		if(x < 0) x = AsteroidsGame.WIDTH;
		if(x > AsteroidsGame.WIDTH) x = 0;
		if(y < 0) y = AsteroidsGame.HEIGHT;
		if(y > AsteroidsGame.HEIGHT) y = 0;
	}
}
