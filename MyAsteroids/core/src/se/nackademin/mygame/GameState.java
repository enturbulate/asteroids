package se.nackademin.mygame;


public abstract class GameState {
	
	protected GameStateHandling gState;
	
	protected GameState(GameStateHandling gState){
		this.gState = gState;
		init();
	}
	//init is called when game first starts
	public abstract void init();
	//calling when we do render/gameloop
	public abstract void update(float dt);
	//same as above
	public abstract void draw();
	//when we do stuff with the game keys
	public abstract void handleInput();
	//when we want to switch game states and get rid of the previous one
	public abstract void dispose();

}
