package se.nackademin.mygame;


public class GameStateHandling {
	
	//current game state
	private GameState gameState;
	
	public static final int MENU = 0;
	public static final int PLAY = 1;  //these variables need to be set to different values, the value itself doesn't matter.
	
	public GameStateHandling(){
		setState(MENU);
	}
	
	public void setState(int state){
		//when we set gameState to a new state, call dispose to get rid of the old state
		if(gameState != null) gameState.dispose();
		if(state == MENU){
			//switch to menu state
			gameState = new MenuState(this);
		}
		if(state == PLAY){
			//switch to play state
			gameState = new PlayState(this);
		}	
	}
	//float dt = Gdx.graphics.GetDeltaTime, is called in render in gameclass
	public void update(float dt){
		gameState.update(dt);
	}
	
	public void draw(){
		gameState.draw();
		
	}

}
