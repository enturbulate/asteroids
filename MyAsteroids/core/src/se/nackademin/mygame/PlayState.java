package se.nackademin.mygame;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;

public class PlayState extends GameState{

	private Ship extraLives;
	private Ship ship;
	private ArrayList<Bullets> bullets;
	private ArrayList<Asteroids> asteroids;
	private ArrayList<Explosions> explosion;
	private SpriteBatch sprite;
	private ShapeRenderer sRenderer;
	private BitmapFont scoreFont;

	private int level;
	

	public PlayState(GameStateHandling gState){
		super (gState);
	}
	public void init() {

		sprite = new SpriteBatch();
		sRenderer = new ShapeRenderer();

		//set font
		FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/slkscr.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 34;
		scoreFont = gen.generateFont(parameter);
		gen.dispose();

		bullets = new ArrayList<Bullets>();

		ship = new Ship(bullets);

		asteroids = new ArrayList<Asteroids>();

		explosion = new ArrayList<Explosions>();

		level = 1;
		spawnAsteroids();

		extraLives = new Ship(null);
	}
	
	private void spawnAsteroids(){

		asteroids.clear();

		//first level is gonna have six asteroids, level 2 eight etc
		int numSpawn = 4 + level +2;

		//forloop to spawn asteroids
		for(int i = 0; i < numSpawn; i++){
			float x = MathUtils.random(AsteroidsGame.WIDTH);
			float y = MathUtils.random(AsteroidsGame.HEIGHT);

			//checking so that the asteroids won't spawn on the ship
			float bx = x - ship.getx();
			float by = y - ship.gety();
			float dist = (float) Math.sqrt(bx * bx + by * by);

			//if the distance is less than 100 pixels of the player,
			//we won't spawn any asteroids
			while(dist < 100){
				//spawn at another random location
				x = MathUtils.random(AsteroidsGame.WIDTH);
				y = MathUtils.random(AsteroidsGame.HEIGHT);
				bx = x - ship.getx();
				by = y - ship.gety();
				dist = (float) Math.sqrt(bx * bx + by * by);
			}

			//when we get out of the whileloop, we've found a proper spot
			//to spawn an asteroid
			asteroids.add(new Asteroids(x, y, Asteroids.LARGE));
		}

	}
	
	
	private void checkCollisions(){
		//if player is hit, the asteroids won't get hit by the explosion
		if(!ship.isHit()){
			//player-asteroid collision
			for(int i =0; i < asteroids.size(); i++) {
				Asteroids a = asteroids.get(i);
				//if player intersects asteroid
				if(a.intersects(ship)){
					ship.hit();
					asteroids.remove(i);
					i--;
					splitAsteroids(a);
					break;
				}
			}
		}

		//bullet-asteroid collision
		for(int i = 0; i < bullets.size(); i++){
			Bullets b = bullets.get(i);
			for(int j = 0; j < asteroids.size(); j++){
				Asteroids a = asteroids.get(j);
				//if asteroid (polygon)a contains the point (bullet) b
				if(a.collisionDetect(b.getx(), b.gety())){
					bullets.remove(i);
					i--;
					asteroids.remove(j);
					j--;
					splitAsteroids(a);
					//increment score
					ship.updateScore(a.getScore());
					break;
				}
			}
		}
	}

	private void createExplosion(float x, float y){
		for(int i = 0; i < 8; i++){
			explosion.add(new Explosions(x, y));
		}
	}

	//split asteroids if hit by bullet, and lower asteroids number left on screen
	private void splitAsteroids(Asteroids a){

		createExplosion(a.getx(), a.gety());

		if(a.getType() == Asteroids.LARGE){
			//add two medium sized asteroids
			asteroids.add(new Asteroids(a.getx(), a.gety(), Asteroids.MEDIUM));
			asteroids.add(new Asteroids(a.getx(), a.gety(), Asteroids.MEDIUM));
		}
		if(a.getType() == Asteroids.MEDIUM){
			asteroids.add(new Asteroids(a.getx(), a.gety(), Asteroids.SMALL));
			asteroids.add(new Asteroids(a.getx(), a.gety(), Asteroids.SMALL));	
		}
	}


	public void update(float dt){

		//get user input
		handleInput();

		//next level
		if(asteroids.size() == 0){
			level++;
			spawnAsteroids();
		}

		//update player
		ship.update(dt);
		if(ship.isDead()){
			if(ship.getLives() == 0){
				gState.setState(GameStateHandling.MENU);
			}
			ship.reset();
			ship.loseLife();
			return;
		}

		//update player bullets
		for(int i = 0; i < bullets.size(); i++){
			bullets.get(i).update(dt);
			if(bullets.get(i).removeBullet()){
				bullets.remove(i);
				i--;
			}
		}

		//update asteroids
		for(int i = 0; i < asteroids.size(); i++){
			asteroids.get(i).update(dt);
			if(asteroids.get(i).shouldRemove()){
				asteroids.remove(i);
				i--;
			}
		}

		//update explosion
		for(int i = 0; i < explosion.size(); i++){
			explosion.get(i).update(dt);
			if(explosion.get(i).removeExplosion()){
				explosion.remove(i);
				i--;
			}
		}
		checkCollisions();

	}

	
	public void draw(){

		//draw player
		ship.draw(sRenderer);

		//draw bullets
		for(int i = 0; i < bullets.size(); i++){
			bullets.get(i).draw(sRenderer);
		}

		//draw asteroids
		for(int i = 0; i < asteroids.size(); i++){
			asteroids.get(i).draw(sRenderer);
		}

		//draw explosions
		for(int i = 0; i < explosion.size(); i++){
			explosion.get(i).draw(sRenderer);
		}

		//draw score
		sprite.setColor(1, 1, 1, 1);
		sprite.begin();
		scoreFont.draw(sprite,Long.toString(ship.getScore()), 30, 580);

		sprite.end();

		//draw lives
		for(int i = 0; i < ship.getLives(); i++){
			extraLives.setPosition(30 + i * 20, 530);
			extraLives.draw(sRenderer);
		}

	}

	public void handleInput(){
		ship.setLeft(GameKeys.isDown(GameKeys.LEFT));
		ship.setRight(GameKeys.isDown(GameKeys.RIGHT));
		ship.setUp(GameKeys.isDown(GameKeys.UP));
		if(GameKeys.isPressed(GameKeys.SPACE)){
			ship.shoot();
		}
	}

	public void dispose(){
		sprite.dispose();
		sRenderer.dispose();
		scoreFont.dispose();
	}

}
