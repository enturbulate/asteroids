package se.nackademin.mygame;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;

public class Explosions extends Objects{
	
	private float explodeTimer;
	private float explodeTime;
	private boolean remove;
	
	public Explosions(float x, float y){
		this.x = x;
		this.y = y;
		width = height = 2;
		
		speed = 50;
		radians = MathUtils.random((int) (2 * Math.PI));
		vdirx = MathUtils.cos(radians) * speed;
		vdiry = MathUtils.sin(radians) * speed;
		
		explodeTimer = 0;
		explodeTime = 1;
	}
	
	public boolean removeExplosion(){
		return remove;
	}
	
	public void update(float dt){
		x += vdirx * dt;
		y += vdiry * dt;
		
		explodeTimer += dt;
		if(explodeTimer > explodeTime){
			remove = true;
		}	
	}
	
	public void draw(ShapeRenderer sr){
		sr.setColor(1, 1, 1, 1);
		sr.begin(ShapeType.Filled);
		sr.circle(x - width / 2, y - width / 2, width / 2);
		sr.end();
	}

}
