package se.nackademin.mygame.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import se.nackademin.mygame.AsteroidsGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new AsteroidsGame(), config);
		
		config.title = "SUPER ASTEROIDS MEGA!";
		config.width = 750;
		config.height = 600;
		//config.useGL30 = false;
		config.resizable = false;
	}
}
